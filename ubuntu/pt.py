import string
import random

def run(stringLength=3):
    word = string.ascii_lowercase
    return ''.join(random.choice(word) for i in range(stringLength))

print(run())
