const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());
const { interface, bytecode } = require('../compile');
var accounts;
var greetings;
beforeEach(async () => {
    accounts = await web3.eth.getAccounts()
    greetings = await
        new web3.eth.Contract(JSON.parse(interface))
            .deploy({
                data: bytecode,
                arguments: ['Hello World']
            }).send({
                from: accounts[0],
                gas: '1000000'
            });
    //console.log(accounts);
});

describe('Greetings', () => {
    it('deploys a greetings contract', () => {
        console.log(greetings);
        assert.ok(greetings.options.address);
        it('message is set by constructor', async () => {
            const message = await
                greetings.methods.message().call();
            assert.equal(message, 'Hello World');
        });
        it('message is set bysetMeesage() ', async () => {
            await greetings.methods.setMessage(
                'Hello Mars').send({ from: accounts[0] });
            const message = await greetings.methods.message().call();
            assert.equal(message, 'Hello Mars');
        });
    });
});