pragma solidity ^0.4.25;
contract Lottery {
    address public manager;
    address[] public players;
    constructor() public {
        manager = msg.sender;
    }
    function enter() public payable {
        require (msg.value >= 0.01 ether);
        players.push(msg.sender);
    }
    function random() private view returns (uint) {
        bytes memory val;
        val = abi.encodePacked(block.difficulty, now, players);
        return uint (keccak256(val));
    }
    function pickWinner() public checkForOnlyManager {
       
        uint index = random() % players.length;
        players[index].transfer(address(this).balance);
        //clear array for next round
        players = new address[](0); 
        //0 heare means we first need the array to have 0 elements, we can enter any number we want
        //for example, if we need this array to have an initial size of 10, we can use 10 instead.
    } 
     modifier checkForOnlyManager {
        require (msg.sender == manager);
        _;  //if require pass, runs the remaining code of the function who use modifier
        //the way solidity works is to put the code of the function that we apply the modifier function 
        //after the reqire() line.
    }
    
    function getPlayers() public view returns (address[]) {
        return players;
    }
}