package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

func (t *SmartContract) Init(stub shim.ChaincodeStubInterface) pb.Response {
	functionName := "[Init]"
	print("==========================================" + functionName + "====================================================")
	println(functionName + " successfully")
	println("===================================================================================" + functionName + "=============================================================================")
	return shim.Success(nil)
}
func (t *SmartContract) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println(function)
	if function == "IssueGarden" {
		return t.IssueGarden(stub, args)
	} else if function == "IssuePlanYearModel" {
		return t.IssuePlanYearModel(stub, args)
	} else if function == "IssuePlanting" {
		return t.IssuePlanting(stub, args)
	} else if function == "IssueManagePlanting" {
		return t.IssueManagePlanting(stub, args)
	} else if function == "IssueHarvest" {
		return t.IssueHarvest(stub, args)
	} else if function == "IssueSelling" {
		return t.IssueSelling(stub, args)
	} else if function == "query" {
		return t.query(stub, args)
	}
	return shim.Error("Invalid invoke function name. Expecting \"IssueGarden\" \"IssuePlanYearModel\" \"IssuePlanting\" \"IssueManagePlanting\" \"IssueHarvest\" \"IssueSelling\"\"query\"\"queryHistory\"")
}

//======================================================== GardenInfo ======================================================================
type Garden_Model struct {
	GardenName           string               `json:"gardenName"`
	Garden               string               `json:"garden"`
	Owner                string               `json:"owner"`
	Areas                string               `json:"areas"`
	History_use_chemical History_use_chemical `json:"history_use_chemical"`
}

type History_use_chemical struct {
	Date_final_use_chemical string `json:"date_final_use_chemical"`
	Status                  string `json:"status"`
}

func (t *SmartContract) IssueGarden(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	functionName := "[IssueGarden]"
	print("==========================================" + functionName + "====================================================")
	println("Input: " + args[0])
	justString := strings.Join(args, "")
	args = strings.Split(justString, "|")
	if len(args) != 6 {
		println("Incorrect number of arguments. Expecting 5")
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	History_use_chemical, err := getHistory_use_chemical(args[5])
	if err != nil {
		println("History_use_chemical" + err.Error())
		return shim.Error("History_use_chemical" + err.Error())
	}
	//date,_ := time.Parse(time.RFC3339,args[2])
	Garden := Garden_Model{
		GardenName:           args[1],
		Garden:               args[2],
		Owner:                args[3],
		Areas:                args[4],
		History_use_chemical: History_use_chemical,
	}
	// check key has already exist??
	println("[CheckKeyExist]")
	GardenKey := "GardenDoc|" + args[0]
	GardenKeyCheck, err := stub.GetState(GardenKey)
	if GardenKeyCheck == nil {
		// Json to ByteArray
		Gardenbytes, err := json.Marshal(Garden)
		if err != nil {
			println("Marshal is error" + err.Error())
			return shim.Error("Marshal is error" + err.Error())
		}
		err = stub.PutState(GardenKey, Gardenbytes)
		if err != nil {
			println("PutState is error" + err.Error())
			return shim.Error("PutState is error" + err.Error())
		}
	}
	if err != nil {
		println("GardenKey " + GardenKey + " has Already Exist" + err.Error())
		return shim.Error("GardenKey " + GardenKey + " has Already Exist" + err.Error())
	}
	println("CheckKeyExist successflly")
	println(functionName + " successfully")
	println("===================================================================================" + functionName + "=============================================================================")
	return shim.Success(nil)
}
func getHistory_use_chemical(get string) (History_use_chemical, error) {
	functionName := "[getHistory_use_chemical]"
	println(functionName)
	var History_use_chemicalAsStruct History_use_chemical
	var jsonData = []byte(get)
	// ByteArray to json
	err := json.Unmarshal(jsonData, &History_use_chemicalAsStruct)
	if err != nil {
		fmt.Printf("There was an error decoding the json. err = %s", err)
	}
	println(functionName + " successfully")
	return History_use_chemicalAsStruct, nil
}

//========================================================  PlanYearModel  ======================================================================
type PlanYearModel struct {
	Update_date string     `json:"update_date"`
	Username    string     `json:"username"`
	Name        string     `json:"name"`
	Group       string     `json:"group"`
	Planting    []Planting `json:"planting_model"`
	Garden      string     `json:"garden"`
}
type Planting struct {
	Plant_id   string `json:"plant_id"`
	Status     string `json:"status"`
	Plant_name string `json:"plant_name"`
}

func (t *SmartContract) IssuePlanYearModel(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	functionName := "[IssuePlanYearModel]"
	println("===================================================================================" + functionName + "=============================================================================")
	println("Input: " + args[0])
	justString := strings.Join(args, "")
	args = strings.Split(justString, "|")
	if len(args) != 7 {
		println("Incorrect number of arguments. Expecting 7")
		return shim.Error("Incorrect number of arguments. Expecting 7")
	}
	// Planting, err := getPlanting(args[5])
	// if err != nil {
	// 	println("Planting" + err.Error())
	// 	return shim.Error("Planting" + err.Error())
	// }

	//strconv.ParseInt (int , base10 ,int64)
	//time.Parse(time.RFC3339, args[?])
	// Date, errParseDate := time.Parse(time.RFC3339, args[1])
	// // t := time.Now()
	// // fmt.Println(t.String())
	// // fmt.Println(2018)
	// println("Date : ")
	// // println(t.Format(Date))
	// if errParseDate != nil {
	// 	println("Date" + errParseDate.Error())
	// 	return shim.Error("Date" + errParseDate.Error())
	// }
	DocPlanYear := PlanYearModel{
		Update_date: args[2],
		Username:    args[3],
		Name:        args[4],
		Group:       args[5],
		Garden:      args[6],
	}
	// check key has already exist??
	GardenKey := "GardenDoc|" + args[1]
	GardenKeyCheck, err := stub.GetState(GardenKey)
	if GardenKeyCheck == nil {
		println("GardenKey " + GardenKey + " does not exist")
		return shim.Error("GardenKey " + GardenKey + " does not exist")
	}
	if err != nil {
		println("getState is error" + err.Error())
		return shim.Error("getState is error" + err.Error())
	}
	println("[CheckKeyExist]")
	PlanYearkey := "PlanYearDoc|" + args[0]
	PlanYearKeycheck, err := stub.GetState(PlanYearkey)

	if PlanYearKeycheck != nil {
		println("PlanYearKey " + PlanYearkey + " has already exist")
		return shim.Error("PlanYearKey " + PlanYearkey + " has already exist")
	}
	if err != nil {
		println("getState is error" + err.Error())
		return shim.Error("getState is error" + err.Error())
	}
	// Json to ByteArray
	PlanYearbytes, err := json.Marshal(DocPlanYear)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	err = stub.PutState(PlanYearkey, PlanYearbytes)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}

	println("CheckKeyExist successflly")
	println(functionName + " successfully")
	println("===================================================================================" + functionName + "=============================================================================")
	return shim.Success(nil)
}

//======================================================== Planting ======================================================================
type Plant_model struct {
	Plant_Id              string                  `json:"plan_id"`
	Plant_date            string                  `json:"plant_date"`
	Plant_name            string                  `json:"plant_name"`
	Seed_type             string                  `json:"seed_type"`
	Predict_harvest       string                  `json:"predict_harvest"`
	Predict_qauntity      int64                   `json:"predict_quantity"`
	Production_activities []Production_activities `json:"production_activities"`
	Harvest               []Harvest               `json:"harvest"`
	Selling               []Selling               `json:"selling"`
}
type Production_activities struct {
	Production_id     int64  `json:"production_id"`
	Production_name   string `json:"production_name"`
	Production_date   string `json:"production_date"`
	Production_factor string `json:"production_factor"`
}
type Harvest struct {
	Harvest_transform_check      string     `json:"harvest_transform_check"`
	Harvesting_product_date_data string     `json:"harvesting_product_date_data"`
	Quantity                     []Quantity `json:"quantity"`
	Total                        []Total    `json:"total"`
}
type Quantity struct {
	Quantity_grade       string `json:"quantity_grade"`
	Quantity_amount      int64  `json:"quantity_amount"`
	Quantity_amount_sell int64  `json:"quantity_amount_sell"`
}
type Total struct {
	Total_grade  string `json:"total_grade"`
	Total_amount int64  `json:"total_amount"`
}
type Selling struct {
	Selling_grade        string `json:"selling_grade"`
	Selling_market_place string `json:"selling_market_place"`
	Selling_amount       int64  `json:"selling_amount"`
}

func (t *SmartContract) IssuePlanting(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	functionName := "[IssuePlanting]"
	println("==========================================" + functionName + "====================================================")
	justString := strings.Join(args, "")
	args = strings.Split(justString, "|")
	if len(args) != 7 {
		println("Incorrect number of arguments. Expecting 7")
		return shim.Error("Incorrect number of arguments. Expecting 7")
	}
	println("Input: " + args[0])
	Plant := Plant_model{
		Plant_Id:   args[3],
		Plant_date: args[4],
		Plant_name: args[5],
		Seed_type:  args[6],
	}
	Plant_planyear := Planting{
		Plant_id:   args[3],
		Status : 	"กำลังดำเนินการ",
		Plant_name: args[5],
	}
	// Plant_garden := History_use_chemical{
	// 	Status	:	"ไม่ว่าง",
	// }
	// CheckKeyNotFound
	// println("[CheckKeyNotFound]")
	PlantyearKey := "PlanYearDoc|" + args[1]
	PlantyearKeyAsByte, err := stub.GetState(PlantyearKey)
	if PlantyearKeyAsByte == nil {
		return shim.Error("PlantyearKey not found ")
	}
	if err != nil {
		println("PlantyearKey is error " + err.Error())
		return shim.Error("PlantyearKey is error " + err.Error())
	}
	// check key has already exist
	println("[CheckKeyExist]")
	PlantKey := "PlantDoc|" + args[0]
	PlantKeyCheck, err1 := stub.GetState(PlantKey)
	if PlantKeyCheck != nil {
		println("PlantKey" + PlantKey + " has Already Exist ")
		return shim.Error("PlantKey " + PlantKey + "has Already Exist ")
	}
	if err1 != nil {
		println("GetState is error" + err1.Error())
		return shim.Error("GetState is error " + err1.Error())
	}
	//check garden
	println("[CheckGardenKey]")
	GardenKey := "GardenDoc|" + args[2]
	GardenKeyCheck, errG:= stub.GetState(GardenKey)
	if GardenKeyCheck == nil {
		return shim.Error("GardenKey not found ")
	}
	if errG != nil {
		println("GardenKey is error " + errG.Error())
		return shim.Error("GardenKey is error " + errG.Error())
	}
	// Json to ByteArray
	Plantbytes, err2 := json.Marshal(Plant)
	if err2 != nil {
		println("Marshal is error " + err2.Error())
		return shim.Error("marshal is error " + err2.Error())
	}
	err3 := stub.PutState(PlantKey, Plantbytes)
	if err3 != nil {
		println("PutState is error" + err3.Error())
		return shim.Error("putState is error " + err3.Error())
	}
	println("PutState is Successfully ")
	println(functionName + "Successfully")
	println("=========================================================================")
	println("Update_plantyear: " + args[1])
	functionName1 := "[Update_plantyear]"
	//ByteArray to Json
	PlanYearModel := PlanYearModel{}
	errUnmarshal := json.Unmarshal(PlantyearKeyAsByte, &PlanYearModel)
	// Validate CCR Document State
	if errUnmarshal != nil {
		//error unmarshaling
		println(functionName1 + " Error unmarshaling CCRDocument:" + errUnmarshal.Error())
		return shim.Error(functionName1 + " Error unmarshaling CCRDocument:" + errUnmarshal.Error())
	}
	//updateplanting
	PlanYearModel.Planting = append(PlanYearModel.Planting, Plant_planyear)
	// Json to ByteArray
	PlantyearKeyAsByte, err = json.Marshal(PlanYearModel)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	//PutState
	err = stub.PutState(PlantyearKey, PlantyearKeyAsByte)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}
	println(functionName1 + " successfully")
	println("===================================================================================" + functionName1 + "=============================================================================")
	//Update Garden
	println("Update_Garden:" + args[2])
	functionName2 := "[Update_Garden]"
	//byteArray to JSON
	Garden_Model := Garden_Model{}
	errUnmarshalG := json.Unmarshal(GardenKeyCheck, &Garden_Model)
	// Validate CCR Document State
	if errUnmarshalG != nil {
		//error unmarshaling
		println(functionName2 + " Error unmarshaling CCRDocument:" + errUnmarshalG.Error())
		return shim.Error(functionName2 + " Error unmarshaling CCRDocument:" + errUnmarshalG.Error())
	}
	// Update Garden
	Garden_Model.History_use_chemical = History_use_chemical{
		Date_final_use_chemical : Garden_Model.History_use_chemical.Date_final_use_chemical,
		Status	:"ไม่ว่าง",}
	//JSON to ByteArray
	GardenKeyCheck, err = json.Marshal(Garden_Model)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	//PutState
	err = stub.PutState(GardenKey, GardenKeyCheck)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}
	println(functionName2 + " successfully")
	println("=======================================" + functionName2 + "========================================")
	
	return shim.Success(nil)
}

//======================================================== IssueManagePlanting ======================================================================
func (t *SmartContract) IssueManagePlanting(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	functionName := "[IssueManagePlanting]"
	print("==========================================" + functionName + "====================================================")
	println("Input: " + args[0])
	justString := strings.Join(args, "")
	args = strings.Split(justString, "|")
	if len(args) != 5 {
		println("Incorrect number of arguments. Expecting 5")
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	Production_id, _ := strconv.ParseInt(args[1], 10, 64)
	PlantActivities := Production_activities{
		Production_id:     Production_id,
		Production_name:   args[2],
		Production_date:   args[3],
		Production_factor: args[4],
	}
	// check key not found??
	println("[Check plantkey not found]")
	PlantKey := "PlantDoc|" + args[0]
	PlantAsByte, _ := stub.GetState(PlantKey)
	if PlantAsByte == nil {
		println("manageKey " + PlantKey + " not found")
		return shim.Error("manageKey " + PlantKey + " not found")
	}
	println("CheckKeyExist successflly")
	//ByteArray to Json
	PlantModel := Plant_model{}
	errUnmarshal := json.Unmarshal(PlantAsByte, &PlantModel)
	// Validate CCR Document State
	if errUnmarshal != nil {
		//error unmarshaling
		println(functionName + " Error unmarshaling CCRDocument:" + errUnmarshal.Error())
		return shim.Error(functionName + " Error unmarshaling CCRDocument:" + errUnmarshal.Error())
	}
	//updateplanting
	PlantModel.Production_activities = append(PlantModel.Production_activities, PlantActivities)
	// Json to ByteArray
	var err error
	PlantAsByte, err = json.Marshal(PlantModel)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	//PutState
	err = stub.PutState(PlantKey, PlantAsByte)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}
	println(functionName + " successfully")
	println("===================================================================================" + functionName + "=============================================================================")

	return shim.Success(nil)
}

//======================================================== IssueHarvest ======================================================================
func (t *SmartContract) IssueHarvest(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	functionName := "[IssueHarvest]"
	print("==========================================" + functionName + "====================================================")
	println("Input: " + args[0])
	justString := strings.Join(args, "")
	args = strings.Split(justString, "|")
	if len(args) != 6 {
		println("Incorrect number of arguments. Expecting 5")
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	Quantity, err := getQuantity(args[4])
	if err != nil {
		println("Quantity" + err.Error())
		return shim.Error("Quantity" + err.Error())
	}
	Total, err := getTotal(args[5])
	if err != nil {
		println("Total" + err.Error())
		return shim.Error("Total" + err.Error())
	}
	harvest := Harvest{
		Harvest_transform_check:      args[2],
		Harvesting_product_date_data: args[3],
		Quantity:                     Quantity,
		Total:                        Total,
	}
	// check key not found??
	println("[Check PlantKey not found]")
	// PlantKey := "" + args[0]
	PlantKey := "PlantDoc|" + args[0]
	PlantAsByte, _ := stub.GetState(PlantKey)
	if PlantAsByte == nil {
		println("ManageKey " + PlantKey + " not found")
		return shim.Error("ManageKey " + PlantKey + " not found")
	}
	// PlantYearKey := "" + args[1]
	PlantYearKey := "PlanYearDoc|" + args[1]
	PlantYearAsByte, _ := stub.GetState(PlantYearKey)
	if PlantYearAsByte == nil {
		println("manageKey " + PlantYearKey + " not found")
		return shim.Error("manageKey " + PlantYearKey + " not found" )
	}
	println("CheckKeyExist successflly")
	//ByteArray to Json
	PlantModel := Plant_model{}
	Plan_year_Model := PlanYearModel{}
	errUnmarshalplanting := json.Unmarshal(PlantAsByte, &PlantModel)
	errUnmarshalplanyear  := json.Unmarshal(PlantYearAsByte, &Plan_year_Model)

	// Validate CCR Document State
	if errUnmarshalplanting != nil {
		//error unmarshaling
		println(functionName + " Error unmarshaling CCRDocument:" + errUnmarshalplanting.Error())
		return shim.Error(functionName + " Error unmarshaling CCRDocument:" + errUnmarshalplanting.Error())
	}
	if errUnmarshalplanyear != nil {
		//error errUnmarshalplanyear
		println(functionName + " Error errUnmarshalplanyear CCRDocument:" + errUnmarshalplanyear.Error())
		return shim.Error(functionName + " Error errUnmarshalplanyear CCRDocument:" + errUnmarshalplanyear.Error())
	}
	//updateplanting
	PlantModel.Harvest = append(PlantModel.Harvest, harvest)
	for i :=0 ;i < len(Plan_year_Model.Planting); i ++ {
		if Plan_year_Model.Planting[i].Plant_id == args[0]{ //args[0] = plantid
			Plan_year_Model.Planting[i].Status = "เก็บเกี่ยวแล้ว"
		}
		println(Plan_year_Model.Planting[i].Plant_id + "=====" + args[0])
	}
	PlantYearAsByte, err = json.Marshal(Plan_year_Model)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	//PutState
	err = stub.PutState(PlantKey, PlantYearAsByte)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}

	//==============================================checktotal
	var total_waste int64 = 0
	var total int64 = 0
	for j := 0; j <= 4; j++ {
		total = total + harvest.Quantity[j].Quantity_amount
	}
	total_waste = harvest.Quantity[5].Quantity_amount
	if total != harvest.Total[0].Total_amount {
		return shim.Error(functionName + "Overload number")
	} else if total_waste != harvest.Total[1].Total_amount {
		return shim.Error(functionName + "Overload number")
	}
	// Json to ByteArray
	PlantAsByte, err = json.Marshal(PlantModel)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	//PutState
	err = stub.PutState(PlantKey, PlantAsByte)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}
	println(functionName + " successfully")
	println("===================================================================================" + functionName + "=============================================================================")

	return shim.Success(nil)
}
func getQuantity(get string) ([]Quantity, error) {
	functionName := "[getQuantity]"
	println(functionName)
	var QuantityAsStruct []Quantity
	var jsonData = []byte(get)
	// ByteArray to json
	err := json.Unmarshal(jsonData, &QuantityAsStruct)
	if err != nil {
		fmt.Printf("There was an error decoding the json. err = %s", err)
	}
	println(functionName + " successfully")
	return QuantityAsStruct, nil
}
func getTotal(get string) ([]Total, error) {
	functionName := "[getTotal]"
	println(functionName)
	var TotalAsStruct []Total
	var jsonData = []byte(get)
	// ByteArray to json
	err := json.Unmarshal(jsonData, &TotalAsStruct)
	if err != nil {
		fmt.Printf("There was an error decoding the json. err = %s", err)
	}
	println(functionName + " successfully")
	return TotalAsStruct, nil
}

//========================================================  SELLING  ======================================================================
type SellingModel struct {
	Data []data `json:"data"`
}
type data struct {
	Plan_product_id           int64                       `json:"plan_product_id"`
	Product_type_id           int64                       `json:"product_type_id"`
	Product_type_unit         string                      `json:"product_type_unit"`
	Id                        int64                       `json:"id"`
	Product_type_name_th      string                      `json:"product_type_name_th"`
	Product_type_name_eng     string                      `json:"product_type_name_eng"`
	Product_type_image_path   string                      `json:"product_type_image_path"`
	Checked                   bool                        `json:"checked"`
	Selling_market_place_data []Selling_market_place_data `json:"selling_market_place_data"`
}
type Selling_market_place_data struct {
	Id              int64   `json:"id"`
	Name            string  `json:"name"`
	Image_path      string  `json:"image_path"`
	Location        string  `json:"location"`
	Latitude        float64 `json:"latitude"`
	Longitude       float64 `json:"longitude"`
	Is_ecommerce    int64   `json:"is_ecommerce"`
	Company_id      int64   `json:"company_id"`
	Checked         bool    `json:"checked"`
	Place_user_name string  `json:"place_user_name"`
	Place_id        int64   `json:"place_id"`
	Quantity        string  `json:"quantity"`
	Amount          string  `json:"amount"`
	Profit          string  `json:"profit"`
}

func (t *SmartContract) IssueSelling(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	functionName := "[IssueSelling]"
	// println("===================================================================================" + functionName + "=============================================================================")
	// println("Input: " + args[0])
	justString := strings.Join(args, "")
	args = strings.Split(justString, "|")
	if len(args) != 5 {
		println("Incorrect number of arguments. Expecting 5")
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	// data, err := getData(args[1])
	// if err != nil {
	// 	println("data: " + err.Error())
	// 	return shim.Error("data: " + err.Error())
	// }
	// DocSelling := SellingModel{
	// 	Data: data,
	// }
	// // check key has already exist??
	// println("[CheckKeyExist]")
	// SellingKey := "SellingDoc|" + args[0]
	// SellingKeycheck, err := stub.GetState(SellingKey)
	// if SellingKeycheck == nil {
	// 	// Json to ByteArray
	// 	Sellingbytes, err := json.Marshal(DocSelling)
	// 	if err != nil {
	// 		println("Marshal is error" + err.Error())
	// 		return shim.Error("Marshal is error" + err.Error())
	// 	}
	// 	err = stub.PutState(SellingKey, Sellingbytes)
	// 	if err != nil {d
	// 		println("PutState is error" + err.Error())
	// 		return shim.Error("PutState is error" + err.Error())
	// 	}
	// } else {
	// 	println("SellingKey " + SellingKey + " has Already Exist")
	// 	return shim.Error("SellingKey " + SellingKey + " has Already Exist")
	// }
	Selling_amount, _ := strconv.ParseInt(args[4], 10, 64)
	DocSelling := Selling{
		Selling_grade:        args[2],
		Selling_market_place: args[3],
		Selling_amount:       Selling_amount,
	}
	// check key has already exist??
	println("[CheckKeyExist]")

	// check Plantkey
	PlantKey := "PlantDoc|" + args[0]
	// get Planting AsByteArray
	Planting, err := stub.GetState(PlantKey)
	if Planting == nil {
		return shim.Error("PlantKey not found ")
	}
	if err != nil {
		println("PlantKey is error " + err.Error())
		return shim.Error("PlantKey is error " + err.Error())
	}

	//check Plantyear
	PlanyearKey := "PlanYearDoc|" + args[1]
	// get Plantyear AsByteArray
	PlanyearKeyAsByte, err := stub.GetState(PlanyearKey)
	if PlanyearKeyAsByte == nil {
		return shim.Error("PlantyearKey not found ")
	}
	if err != nil {
		println("PlanyearKey is error " + err.Error())
		return shim.Error("PlanyearKey is error " + err.Error())
	}
	functionName1 := "[Update_planting]"
	PlantingModel := Plant_model{}
	errUnmarshal := json.Unmarshal(Planting, &PlantingModel)
	if errUnmarshal != nil {
		//error unmarshaling
		println(functionName1 + " Error unmarshaling CCRDocument:" + errUnmarshal.Error())
		return shim.Error(functionName1 + " Error unmarshaling CCRDocument:" + errUnmarshal.Error())
	}
	// update Planting
	PlantingModel.Selling = append(PlantingModel.Selling, DocSelling)
	Planting, err = json.Marshal(PlantingModel)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	// Json to ByteArray
	Sellingbytes, err := json.Marshal(PlantingModel)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	err = stub.PutState(PlantKey, Sellingbytes)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}
	println(functionName1 + " successfully")

	functionName2 := "[Update_planyear]"
	PlanYearModel := PlanYearModel{}
	errUnmarshalPlanyear := json.Unmarshal(PlanyearKeyAsByte, &PlanYearModel)
	// Validate CCR Document State
	if errUnmarshalPlanyear != nil {
		//error unmarshaling
		println(functionName1 + " Error unmarshaling CCRDocument:" + errUnmarshalPlanyear.Error())
		return shim.Error(functionName1 + " Error unmarshaling CCRDocument:" + errUnmarshalPlanyear.Error())
	}
	//updateplanting
	// PlanYearModel.Planting = append(PlanYearModel.Planting, Planting)

	for i := 0; i < len(PlanYearModel.Planting); i++ {
		if PlanYearModel.Planting[i].Plant_id == args[0] { //args[0] = plantid
			PlanYearModel.Planting[i].Status = "ขายแล้ว"
		}
	}
	// Json to ByteArray
	PlanyearKeyAsByte, err = json.Marshal(PlanYearModel)
	if err != nil {
		println("Marshal is error" + err.Error())
		return shim.Error("Marshal is error" + err.Error())
	}
	//PutState
	err = stub.PutState(PlanyearKey, PlanyearKeyAsByte)
	if err != nil {
		println("PutState is error" + err.Error())
		return shim.Error("PutState is error" + err.Error())
	}
	println(functionName2 + " successfully")

	println("CheckKeyExist successflly")
	println(functionName + " successfully")
	println("===================================================================================" + functionName + "=============================================================================")
	return shim.Success(nil)
}
func getData(get string) ([]data, error) {
	functionName := "[getData]"
	println(functionName)
	var dataAsStruct []data
	var jsonData = []byte(get)
	// ByteArray to json
	err := json.Unmarshal(jsonData, &dataAsStruct)
	if err != nil {
		fmt.Printf("There was an error decoding the json. err = %s", err)
	}
	println(functionName + " successfully")
	return dataAsStruct, nil
}

//========================================================  query  ======================================================================
func (t *SmartContract) query(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var A string // Entities
	var err error
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}
	A = args[0]
	// Get the state from the ledger
	Avalbytes, err := stub.GetState(A)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + A + "\"}"
		return shim.Error(jsonResp)
	}
	if Avalbytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + A + "\"}"
		return shim.Error(jsonResp)
	}
	jsonResp := "{\"Name\":\"" + A + "\",\"Amount\":\"" + string(Avalbytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return shim.Success(Avalbytes)
}

//========================================================  query queryHistory  ======================================================================
func (t *SmartContract) queryHistory(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	keyname := args[0]

	fmt.Printf("- start getHistoryForMarble: %s\n", keyname)

	resultsIterator, err := stub.GetHistoryForKey(keyname)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing historic values for the marble
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		// if it was a delete operation on given key, then we need to set the
		//corresponding value null. Else, we will write the response.Value
		//as-is (as the Value itself a JSON marble)
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		buffer.WriteString("\"")

		buffer.WriteString(", \"IsDelete\":")
		buffer.WriteString("\"")
		buffer.WriteString(strconv.FormatBool(response.IsDelete))
		buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForMarble returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

type SmartContract struct {
}

func main() {
	// // Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
