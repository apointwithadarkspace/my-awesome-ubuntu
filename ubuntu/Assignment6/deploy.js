const Web3 = require("web3");
//const web3 = new Web3();
const HDWalletProvider = require('truffle-hdwallet-provider');
const provider = new HDWalletProvider('boss blush ill quit swamp during despair chimney hazard canyon timber second','https://rinkeby.infura.io/v3/39f100cb0d21467fb5fb16f79a49f6c9');
const web3 = new Web3(provider)
/*web3.setProvider(new
web3.providers.HttpProvider('http://localhost:8545'));*/
const { interface, bytecode } = require('./compile');
const deploy = async () => {
    accounts = await web3.eth.getAccounts();
    console.log(accounts);
    lottery = await
    new web3.eth.Contract(JSON.parse(interface))
    .deploy({
        data: '0x'+bytecode,
        arguments: []
    }).send({
        from: accounts[0],
        gas:'1000000'
    });
    //console.log(interface);
    console.log('contract deployed to',lottery.options.address);
    /*const message = await
        greetings.methods.message().call();
    console.log(message);*/
};
deploy();