const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const provider = ganache.provider();
const web3 = new Web3(provider);
const { interface, bytecode } = require('../compile');
var accounts;
var lottery;
beforeEach(async () => {
    accounts = await web3.eth.getAccounts()
    lottery = await
        new web3.eth.Contract(JSON.parse(interface))
            .deploy({
                data: bytecode

            }).send({
                from: accounts[0],
                gas: '1000000'
            });

    console.log(accounts);

});

describe('Lottery', () => {
    it('deploys a Lottery contract', () => {
        console.log('contract: ' + lottery.options.address);
        assert.ok(lottery.options.address);

    });

});

it('test one account to enter ', async () => {
    await lottery.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei("0.01", "ether")
    });
    const players = await lottery.methods.getPlayers().call({
        from: accounts[0]
    });
    assert.equal(accounts[0], players[0]);
    assert.equal(1, players.length);
});

it('test multiple account to enter ', async () => {
    await lottery.methods.enter().send({
        from: accounts[0],
        value: web3.utils.toWei("0.01", "ether")
    });
    await lottery.methods.enter().send({
        from: accounts[1],
        value: web3.utils.toWei("0.01", "ether")
    });
    await lottery.methods.enter().send({
        from: accounts[2],
        value: web3.utils.toWei("0.01", "ether")
    });
    const players = await lottery.methods.getPlayers().call({
        from: accounts[0]
    });
    assert.equal(accounts[0], players[0]);
    assert.equal(accounts[1], players[1]);
    assert.equal(accounts[2], players[2]);
    assert.equal(3, players.length);
});

it('requires minimum amount of ether to enter', async () => {
    var pass = "ok"
    try {
        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei("0", "ether")
        });

    } catch (err) {
        pass = "not ok";

    }
    assert.equal("not ok", pass);
});

it('only manager can call pickWiner', async () => {
    var pass = "ok";
    try {
        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei("0.01", "ether")
        });
        await lottery.methods.pickWinner().send({
            from: accounts[1]
        });
    }
    catch (err) {
        pass = "not ok";
    }
    assert.equal("not ok", pass);
});